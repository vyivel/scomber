import soundfile as sf
import numpy as np
from scipy import signal

import warnings


class Matcher:
    def __init__(self, path: str, padding: int, rate: int):
        data, orig_rate = sf.read(path)
        length = len(data) + padding

        # Convert to mono
        if len(data.shape) > 1:
            data = np.mean(data, axis=1)
        # Resample
        if orig_rate != rate:
            n_samples = int(np.ceil(len(data) * rate / orig_rate))
            data = signal.resample(data, n_samples)
        # Normalize
        data = (data - np.mean(data)) / (np.std(data) * np.sqrt(length * len(data)))

        self.path = path
        self.length = length
        self.data = data

        self.clear()

    def clear(self):
        self.buf = np.zeros(self.length, dtype=np.float32)

    def feed(self, data: np.ndarray) -> float:
        start = self.length - len(data)
        self.buf[:start] = self.buf[len(data) :]
        self.buf[start:] = data
        with warnings.catch_warnings():
            warnings.simplefilter("ignore")
            # RuntimeWarning: invalid value encountered in divide
            norm = (self.buf - np.mean(self.buf)) / np.std(self.buf)
            # RuntimeWarning: Use of fft convolution on input with
            # NAN or inf results in NAN or inf output.
            corr = signal.correlate(norm, self.data, method="fft", mode="valid")
        corr = np.max(corr)
        return corr
