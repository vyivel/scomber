import pyaudiowpatch as pyaudio
import numpy as np
import keyboard

from logger import logger, init_logger
from context import ctx
from config import cfg


def main():
    init_logger()

    logger.info("Loading configuration...")
    cfg.load("config.ini")

    logger.info("Initializing...")
    with pyaudio.PyAudio() as audio:
        try:
            wasapi_info = audio.get_host_api_info_by_type(pyaudio.paWASAPI)
        except OSError:
            logger.fatal("Failed to get WASAPI info!")
            return

        device = audio.get_device_info_by_index(wasapi_info["defaultOutputDevice"])

        if not device["isLoopbackDevice"]:
            # Fallback
            for loopback in audio.get_loopback_device_info_generator():
                if device["name"] in loopback["name"]:
                    device = loopback
                    break
            else:
                logger.fatal("Failed to find a device!")
                return

        logger.info(f"Audio device: {device['name']}")

        channels = device["maxInputChannels"]
        rate = int(device["defaultSampleRate"])

        ctx.set_rate(rate)
        ctx.load_matchers()

        def stream_callback(data, *_):
            data = np.frombuffer(data, dtype=np.float32)
            if channels > 1:
                # Convert to mono
                data = data.reshape(-1, channels)
                data = np.mean(data, axis=1)
            ctx.push_chunk(data)
            return (None, pyaudio.paContinue)

        logger.info(f"Ready! Press {cfg.bind_start} to start.")

        keyboard.wait(cfg.bind_start)
        keyboard.add_hotkey(cfg.bind_stop, ctx.terminate)

        with audio.open(
            format=pyaudio.paFloat32,
            rate=rate,
            channels=channels,
            input=True,
            input_device_index=device["index"],
            frames_per_buffer=ctx.window,
            stream_callback=stream_callback,
        ):
            ctx.process()


if __name__ == "__main__":
    try:
        main()
    except KeyboardInterrupt:
        logger.info(f"Interrupted!")
        ctx.terminate()
