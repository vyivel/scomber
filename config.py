import math
from configparser import ConfigParser

from logger import logger


class Config:
    def error(section: str, option: str, msg: object):
        logger.error(f"[{section}] {option}: {msg}")

    def load_float(
        data: ConfigParser,
        section: str,
        option: str,
        fallback: float,
        f_min: float = 0,
        f_max: float = math.inf,
    ):
        try:
            n = data.getfloat(section, option, fallback=fallback)
        except ValueError:
            Config.error(section, option, f"not a valid number")
            return fallback
        if n <= f_min or n >= f_max:
            Config.error(section, option, f"not in ({f_min}, {f_max}) range")
            return fallback
        return n

    def load_keycode(
        data: ConfigParser,
        section: str,
        option: str,
        fallback: float,
    ):
        try:
            n = data.getint(section, option, fallback=fallback)
        except ValueError:
            Config.error(section, option, f"not a valid number")
            return fallback
        if n <= 0:
            Config.error(section, option, f"not a valid keycode")
            return fallback
        return n

    def __init__(self):
        self.bind_start: str = None
        self.bind_stop: str = None

        # 0..1
        self.matcher_threshold: float = None
        self.matcher_dir: str = None
        # In seconds
        self.matcher_window: float = None
        self.matcher_padding: float = None

        self.timing_match_timeout: float = None
        self.timing_wait: float = None
        self.timing_hold_down: float = None
        self.timing_hold_up: float = None

        self.bind_action: str = None
        self.bind_down: str = None
        self.bind_up: str = None

    def load(self, path: str):
        data = ConfigParser()
        data.read(path)

        self.bind_start = data.get("binds", "start", fallback="f10")
        self.bind_stop = data.get("binds", "stop", fallback="f10")

        self.keycode_action = Config.load_keycode(
            data, "keycodes", "action", fallback=28
        )
        self.keycode_down = Config.load_keycode(data, "keycodes", "down", fallback=208)
        self.keycode_up = Config.load_keycode(data, "keycodes", "up", fallback=200)

        self.matcher_dir = data.get("matcher", "dir", fallback="sfx")
        self.matcher_threshold = Config.load_float(
            data, "matcher", "threshold", 0.6, f_max=1
        )
        self.matcher_window = Config.load_float(data, "matcher", "window", 0.02)
        self.matcher_padding = Config.load_float(data, "matcher", "padding", 0.1)

        self.timing_match_timeout = Config.load_float(
            data, "timing", "match_timeout", 30
        )
        self.timing_wait = Config.load_float(data, "timing", "wait", 15)
        self.timing_hold_down = Config.load_float(data, "timing", "hold_down", 2)
        self.timing_hold_up = Config.load_float(data, "timing", "hold_up", 0.25)


cfg = Config()
