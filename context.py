import sys
import os
from queue import Empty, Queue
from threading import Condition, Thread
import time

import numpy as np

from matcher import Matcher
from controller import Controller
from logger import logger
from config import cfg


class State:
    LISTENING = 0
    REELING = 1


class Context:
    def __init__(self):
        # Initialized at runtime
        self.rate = None
        self.matchers = []

        # Computed; in samples
        self.padding = None
        self.window = None

        # Internal state
        self.running = True
        self.chunks = Queue()
        self.match_cond = Condition()
        self.state = State.LISTENING

    def set_rate(self, rate: int):
        self.rate = rate
        self.padding = int(cfg.matcher_padding * rate)
        self.window = int(rate * cfg.matcher_window)

    def load_matchers(self):
        absolute = os.path.abspath(cfg.matcher_dir)
        logger.info(f"Loading matchers from {absolute}")
        for dirpath, _, names in os.walk(cfg.matcher_dir):
            for name in names:
                path = os.path.join(dirpath, name)
                matcher = Matcher(path, self.padding, self.rate)
                logger.info(f"Loaded {path}")
                self.matchers.append(matcher)
        if not self.matchers:
            logger.fatal("No files to match found!")
            sys.exit(1)

    def push_chunk(self, chunk: np.ndarray):
        self.chunks.put(chunk)

    def terminate(self):
        self.running = False
        logger.info("Waiting for the process thread to stop...")

    def process(self):
        self.cast()

        while ctx.running:
            try:
                chunk = self.chunks.get(timeout=1)
            except Empty:
                continue
            if self.state != State.LISTENING:
                continue
            for matcher in self.matchers:
                corr = matcher.feed(chunk)
                if corr >= cfg.matcher_threshold:
                    logger.info(f"Matched {matcher.path}: corr = {corr:.3f}")
                    with self.match_cond:
                        self.match_cond.notify_all()
                    break

        logger.info("Bye!")

    def listen(self):
        logger.info("Listening!")
        self.state = State.LISTENING
        for matcher in self.matchers:
            matcher.clear()

        def target():
            with self.match_cond:
                if not self.match_cond.wait(timeout=cfg.timing_match_timeout):
                    logger.info("Listening timeout!")
            if self.running:
                self.reel()

        Thread(target=target, daemon=True).start()

    def cast(self):
        logger.info("Casting!")
        Controller.cast()
        self.listen()

    def reel(self):
        logger.info("Reeling!")
        self.state = State.REELING
        Controller.reel()

        def target():
            time.sleep(cfg.timing_wait)
            if self.running:
                self.cast()

        Thread(target=target, daemon=True).start()


ctx = Context()
