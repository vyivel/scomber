# Scomber

A fishing bot for _NieR:Automata_.

## Dependencies

- `numpy`
- `scipy`
- `soundfile`
- `keyboard`
- [`PyAudioWPatch`](https://github.com/s0d3s/PyAudioWPatch)

## Configuration

See `config.ini`. The configuration file must be located in the directory
where the program is executed.

`dir` specified in the `matchers` section must contain the sound effects to listen for.

### Obtaining the sound effects

1. Extract `bg/bga104.dat` from `data/data005.cpk` with [CriPakTools](https://github.com/wmltogether/CriPakTools).
2. Extract `bga104.bnk` from `bga104.dat`. A variety of tools is available for that;
   I made [pgdat](https://codeberg.org/vyivel/pgdat).
3. Extract `.wem` files from `bga104.bnk` with [NieR-Audio-Tools](https://github.com/NSACloud/NieR-Audio-Tools). Files that you need:
   - `bga104_fishing_hit {r}-135023813.wem`
   - `bga104_fishing_hit {r}-625222250.wem`
   - `bga104_fishing_hit {r}-710613935.wem`
4. Convert the required `.wem` files to `.ogg` files with [ww2ogg](https://github.com/hcs64/ww2ogg). Note: you'll have to use alternate packed codebooks.
5. Fix `.ogg` files with [ReVorb](https://github.com/ItsBranK/ReVorb).


## Usage

```
python main.py
```

1. Run the program, wait for it to become ready.
2. Find a suitable spot for fishing.
3. Start fishing.
4. Start the main loop by pressing F10\*.
5. Do something more useful that pressing a button once in a while. (optional)
6. Stop the main loop by pressing F10\*.

\*by default

## License

BSD-2-Clause

See `LICENSE` for more information.

## Other

Prior art: [nierfishingbot](https://github.com/dwightguth/nierfishingbot) by Dwight Guth